# STATS 506 proj


## Name
Climate Change: Sea Level Rise in Boston Area

## Description
This project is going to use some geographical variables to predict the sea level in Boston (monthly). The aim of us is to interpret how the variable can influence the sea level and which variables are most important (significant) in our models. 

In this project, we will use linear regression, ridge/lasso regression, neural network，random forest and gradient boosting. 


## Usage
Our raw and cleaned data are in the \\data directory. To preprocess the data, the Data_Processing does all the work. The model of linear regression is in Linear_Regression.Rmd, the ridge/lasso regression and neural network are in STATS506_Ridge_Lasso_NN.Rmd. The random forest model is in STATS 506 proj azy.Rmd, and the gradient boosting model is in proj_jhc.Rmd. The project can be reproduced by running those R Markdown files. Some plots showing the results and the report are in \\doc. 


